import random
import statsd
import time

statsd_client = statsd.StatsClient('graphite', 8125) #, prefix='my_prefix.')

while True:
    #timer = random.randint(5, 10)
    time.sleep(0.1)
    statsd_client.incr('metric1', count=random.randint(5, 10000))
    statsd_client.timing("my_timing", 100.1)
